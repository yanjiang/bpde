# BPDE-dG sequencing data analysis

The bash script "BPDE-dG_paired-end.sh" was used to process raw data

**The output bedgraph file using "BPDE-dG_pairedend.sh" can be unioned using the unionbedg function from bedtools:**

`bedtools unionbedg -g hg38.chrom.genome -empty -i file_1.density file_2.density -header -names name_1 name_2 > Site_density.table`

**The unified table can be analyzed using R.** 

The DNA logo was plotted using "R_logo.R" in "script" folder 

The genome-wide karyogram, scatter plots, and the correlation figures were plotted using R script under "R_analysis_using_bin_data.R". Example data were attached in folder ExampleFiles 

**Methylation** 
Merged_LMRs.bed contains the common non- and partly methylated regions appeared in all samples.
Merged_FMRs.bed contains the common fully methylated regions.

