# load packages####

library(rtracklayer)
library(ggbio) 
library(GenomicRanges) 
library(gginnards)
library(BRGenomics)
library(ggnewscale)
library(dplyr)
library(reshape2)
library(readr)
library(DESeq2)



# load data ####

# load density 
site_density <- read.delim("Site_density.table", header = TRUE, sep = "\t")
coordinate <- site_density[,1:3]
site_density[site_density == 0] <- NA
site_density[,1:3] <- coordinate

# load genomic feature  
Features <- read.delim("Gfeatures.table", header = TRUE, sep = "\t")
Features[Features == 0] <- NA
Features[,1:3] <- coordinate

# calculate sizefactor  ####

density_matrix <- data.matrix(site_density[,4:ncol(site_density)])
density_matrix[is.na(density_matrix)] <- 0
rownames(density_matrix) <- paste(site_density$chrom,site_density$start,site_density$end,sep = "_")

col <- data.frame(colnames(density_matrix))
col$condition <- c(rep(c("control","cell_0.25","cell_0.5","cell_1","cell_2","gDNA_2"), each=3))

coldata <- as.matrix(col$condition)
rownames(coldata) <- col$colnames.density_matrix.
colnames(coldata) <- "condition"

dds <- DESeqDataSetFromMatrix(countData = density_matrix,colData = coldata, design = ~ condition)
dds <- estimateSizeFactors(dds)

scaled_density <- coordinate

scaled_density[,4:ncol(site_density)] <- counts(dds,normalized =TRUE)
colnames(scaled_density) <- colnames(site_density)

#calculate the mean of samples ####
mean_density <- coordinate
mean_density$Control.mean <- (scaled_density[,4]+scaled_density[,5]+scaled_density[,6])/3
mean_density$Cell_0.25.mean <- (scaled_density[,7]+scaled_density[,8]+scaled_density[,9])/3
mean_density$Cell_0.5.mean <- (scaled_density[,10]+scaled_density[,11]+scaled_density[,12])/3
mean_density$Cell_1.mean <- (scaled_density[,13]+scaled_density[,14]+scaled_density[,15])/3
mean_density$Cell_2.mean <- (scaled_density[,16]+scaled_density[,17]+scaled_density[,18])/3
mean_density$gDNA_2.mean <- (scaled_density[,19]+scaled_density[,20]+scaled_density[,21])/3


#data preparation for karyogram  #####

#hg38_seqinfo <-Seqinfo(genome = "hg38")
hg38_seqinfo <- BSgenome.Hsapiens.UCSC.hg38::BSgenome.Hsapiens.UCSC.hg38@seqinfo

chromosomes <- c("chr1", "chr2", "chr3", "chr4", "chr5", "chr6", "chr7", "chr8", 
                 "chr9", "chr10", "chr11", "chr12", "chr13", "chr14", "chr15","chr16", 
                 "chr17", "chr18", "chr19", "chr20", "chr21", "chr22", "chrX")


#calculate Log2 ratio, in this case between Cell and gDNA exposed to 2 μM BPDE 

Log2_density <- coordinate
Log2_density$score <- log2(mean_density$Cell_2.mean/mean_density$gDNA_2.mean)
Gfeature <- coordinate
Gfeature$score <- Features$DNase

#make GRange object 

Log2_density_Grange <- makeGRangesFromDataFrame(Log2_density,
                                                keep.extra.columns=TRUE,
                                                ignore.strand=T,
                                                seqinfo=NULL)
Gfeature_Grange <- makeGRangesFromDataFrame(Gfeature, keep.extra.columns=TRUE,
                                            ignore.strand=T,
                                            seqinfo=NULL)

tidy_Log2_density_Grange <- tidyChromosomes(Log2_density_Grange, 
                                                     keep.X = TRUE, 
                                                     keep.Y = FALSE, 
                                                     keep.M = FALSE,
                                                     keep.nonstandard = FALSE, 
                                                     genome = "hg38")

tidy_Gfeature_Grange <- tidyChromosomes(Gfeature_Grange, keep.X = TRUE, 
                                        keep.Y = FALSE, 
                                        keep.M = FALSE,
                                        keep.nonstandard = FALSE, 
                                        genome = "hg38")

#add seqinfo 
tidy_Log2_density_Grange@seqinfo <- hg38_seqinfo[names(seqinfo(tidy_Log2_density_Grange))]
tidy_Log2_density_Grange <- trim(tidy_Log2_density_Grange)

tidy_Gfeature_Grange@seqinfo <- hg38_seqinfo[names(seqinfo(tidy_Gfeature_Grange))]
tidy_Gfeature_Grange <- trim(tidy_Gfeature_Grange)

#group data by percentile (score<0.001, 0.001<score<0.999, score>0.999)

tidy_Log2_density_Grange$score[is.infinite(tidy_Log2_density_Grange$score)] <- NA
tidy_Log2_density_Grange$score[is.na(tidy_Log2_density_Grange$score)] <- 0
percentile_Log2_density_Grange <- tidy_Log2_density_Grange[quantile(tidy_Log2_density_Grange$score, 0.999) > tidy_Log2_density_Grange$score 
                                                                             & quantile(tidy_Log2_density_Grange$score, 0.001) < tidy_Log2_density_Grange$score]
tidy_Gfeature_Grange$score[is.na(tidy_Gfeature_Grange$score)] <- 0
percentile_Gfeature_Grange <- tidy_Gfeature_Grange[quantile(tidy_Gfeature_Grange$score, 0.999) > tidy_Gfeature_Grange$score 
                                             & quantile(tidy_Gfeature_Grange$score, 0.001) < tidy_Gfeature_Grange$score]
extrem_high_Log2_density_Grange <- tidy_Log2_density_Grange[quantile(tidy_Log2_density_Grange$score, 0.999) < tidy_Log2_density_Grange$score]
extrem_low_Log2_density_Grange <- tidy_Log2_density_Grange[quantile(tidy_Log2_density_Grange$score, 0.001) > tidy_Log2_density_Grange$score]

percentile_Gfeature_Grange$score[percentile_Gfeature_Grange$score==0] <- NA
tidy_Gfeature_Grange$score[tidy_Gfeature_Grange$score==0] <- NA
tidy_Log2_density_Grange$score[tidy_Log2_density_Grange$score==0] <- NA
percentile_Log2_density_Grange$score[percentile_Log2_density_Grange$score==0] <- NA
extrem_high_Log2_density_Grange$score[extrem_high_Log2_density_Grange$score==0] <- NA
extrem_low_Log2_density_Grange$score[extrem_low_Log2_density_Grange$score==0] <- NA


colnames(mcols(percentile_Log2_density_Grange)) <- "Log2Ratio"
colnames(mcols(extrem_high_Log2_density_Grange)) <- "HighScores"
colnames(mcols(extrem_low_Log2_density_Grange)) <- "LowScores"
colnames(mcols(percentile_Gfeature_Grange)) <- "Coverage"
colnames(mcols(tidy_Gfeature_Grange)) <- "Coverage"

#plot ####

regions <- seqinfo(percentile_Log2_density_Grange)

autoplot(regions, layout = "karyogram") +
  layout_karyogram(data = percentile_Log2_density_Grange, aes(fill = Log2Ratio, color = Log2Ratio),ylim = c(0,10)) +
  scale_fill_gradient2(low ="lightblue", high= "red",mid = "white",midpoint = 0, na.value = "#f2f2f2") +
  scale_color_gradient2(low ="lightblue", high= "red",mid = "white",midpoint = 0, na.value = "#f2f2f2")  + 
  new_scale_fill() + 
  new_scale_color() + 
  layout_karyogram(data = extrem_low_Log2_density_Grange, aes(fill = LowScores, color = LowScores),ylim = c(0,10)) +
  scale_fill_gradient("LowScores", low ="blue", high= "blue", na.value = "#f2f2f2",breaks =NULL) +
  scale_color_gradient("LowScores", low ="blue", high= "blue", na.value = "#f2f2f2",breaks =NULL) +
  new_scale_fill() + 
  new_scale_color() + 
  layout_karyogram(data = extrem_high_Log2_density_Grange, aes(fill = HighScores, color = HighScores),ylim = c(0,10)) +
  scale_fill_gradient("HighScores", low ="red", high= "red", na.value = "#f2f2f2",breaks =NULL) +
  scale_color_gradient("HighScores", low ="red", high= "red", na.value = "#f2f2f2",breaks =NULL) +
  new_scale_fill() + 
  new_scale_color() + 
  layout_karyogram(data = tidy_Gfeature_Grange, aes(fill = Coverage, color = Coverage),ylim = c(0,2)) +
  scale_fill_gradient("Coverage", low ="white",high= "black",na.value = "#f2f2f2") +
  scale_color_gradient("Coverage", low ="white",high= "black",na.value = "#f2f2f2") +
  theme(panel.background = element_blank(),rect = element_blank(),
        legend.position = c(.8,.5),legend.text = element_text(size = 8),
        legend.title = element_blank()) +  
  labs(title= "Relative Abundance",
       subtitle = "Cell exposed to 2μM BPDE", x =NULL) 


# plot scatter plot ####

# load packages
library(ggpubr)


#scatter plot, here use read count vs GC as an example 

scat <- coordinate
scat$features <- Features$GC
scat$sample <- mean_density$gDNA_2.mean

scat[is.infinite(scat$sample)] <- NA
scat <- na.omit(scat)

#optional
#scat_cleaned <- scat[nchar(scat$Chrom)<=5,]
#scat_cleaned_noMT <- scat_cleaned[(scat_cleaned$Chrom!="chrM" & scat_cleaned$Chrom!="chrMT"),]


ggscatter(scat, y = "sample", x = "features",size = 1, color = "white",
          add.params = list(color = "blue", fill = "lightgray"), 
          conf.int = TRUE, cor.coef = TRUE, cor.coef.coord = c(25,300),
          cor.coeff.args = list(method = "spearman",label.sep = "\n",size = 6)) +
  scale_x_continuous(limits = c(25,65)) +
  scale_y_continuous(limits = c(0.1,350)) +
  geom_bin_2d(bins= 100,show.legend = TRUE) +
  #or use geom_hex, but there's an issue with geom_hex in ggplot 3.4.0
  scale_fill_gradientn(colours = c("navy", "blue","yellow"),trans="log10" ) +
  labs(fill='Bin density', y= "read count", x= "GC (%)") +
  theme(axis.title = element_text(color = "black", size = 18,vjust = 0),
        axis.text = element_blank(),axis.ticks = element_blank(),
        panel.border = element_rect(colour = "black", fill=NA, size=1),
        axis.text.x = element_text(color = "black", size = 20),
        axis.text.y = element_text(color = "black", size = 20),
        axis.title.x = element_text(color = "black", size = 20),
        axis.title.y = element_text(color = "black", size = 20),
        plot.title = element_text(hjust = 0.5,vjust = 2, size = 18),
        legend.title = element_blank(),
        plot.margin=unit(c(1,1,1.5,1.2),"cm"),
        legend.position = "right",
        legend.direction = "vertical") 


# summarized correlations with error bars ####

# load packages
library(Hmisc)


# calculate relative abundance using the triplicate gDNA samples
df1 <- scaled_density[,4:ncol(scaled_density)]
df2 <- log2(df1[,1:15]/df1$gDNA_2uM_1)
df2 <- cbind(df2,log2(df1[,1:15]/df1$gDNA_2uM_2))
df2 <- cbind(df2,log2(df1[,1:15]/df1$gDNA_2uM_3))

df2$feature <- Features$GC
df2_matrix <- as.matrix(df2)
df2_matrix[is.infinite(df2_matrix)]  <- NA

spearman <- rcorr(na.omit(df2_matrix),type = "spearman") 

#calculate mean and std from replicates
df3 <- spearman$r["feature",]
df_r <- data.frame(df3[c(1:3,16:18,31:33)])
colnames(df_r) <- '0'
df_r$'0.25' <- as.vector(df3[c(4:6,19:21,34:36)])
df_r$'0.5' <- as.vector(df3[c(7:9,22:24,37:39)])
df_r$'1' <- as.vector(df3[c(10:12,25:27,40:42)])
df_r$'2' <- as.vector(df3[c(13:15,28:30,43:45)])
df_r <- data.frame(t(df_r))
df_r$mean <- as.vector(rowMeans(df_r))
df_r$std <- apply(df_r[,1:9],1, sd, na.rm = TRUE)
df_r$samples <- rownames(df_r)
df_r$samples <- factor(df_r$samples, levels = dput(df_r$samples))

# plot
ggplot(df_r, aes(x = samples, y = mean, fill=samples)) +
  geom_bar(stat="identity", width=0.63,colour="black") +
  scale_fill_manual(values = rep("darkgrey",5))+
  geom_errorbar(aes(ymin=mean-std, ymax=mean+std), width=.2,size = 1,color = "black") +
  ylab("Spearman coefficient") + 
  theme(panel.background = element_blank(),
        panel.grid.major.y = element_line(colour = "lightgrey",linetype = "dashed"),
        axis.title = element_text(color = "black", size = 18,vjust = 0),
        axis.text = element_blank(),axis.ticks = element_blank(),
        axis.text.x = element_text(color = "black", size = 20),
        axis.text.y = element_text(color = "black", size = 20),
        axis.title.x = element_blank(),
        axis.title.y = element_text(color = "black", size = 20,vjust = 3),
        legend.position = "none",
        plot.margin=unit(c(1,1,1.5,1.2),"cm")) +
  scale_y_reverse()



