# load library ######################
library(tibble)
library(ggplot2)
library(ggseqlogo)
library(readr)

# load file ###################
temps = list.files(pattern = "*_PWM.txt")
list.PWM <- list()

#notice the positiion of interested base, if necessary, change the last line in loop#############
for (i in 1:length(temps))
{
  list.PWM[[i]]<-read.delim(file= temps[i], header=FALSE, sep="\t")
  colnames(list.PWM[[i]]) <- c("Base",-10:10)
  list.PWM[[i]] <- list.PWM[[i]][list.PWM[[i]][,2] > 0.1,]
#  list.PWM[[i]] <- list.PWM[[i]][,11:13]
}

matrix.PWM <- list()
name.list <- list()
for (i in 1:length(temps))
{
  matrix.PWM[[i]] <- data.matrix(list.PWM[[i]][2:22])
}

#depends on file name need to change if necessary
for (i in 1:length(temps))
{
  row.names(matrix.PWM[[i]]) <- c("A","C","G","T")
  name.list[i] <- substr(temps[i], 1, nchar(temps[i])-11)
}
names(matrix.PWM) <- name.list

ggseqlogo(matrix.PWM, font="roboto_medium", method = 'prob') +
  annotate('rect', xmin = 10.5, xmax = 11.5, ymin = -0.0003, ymax = 1.0003, alpha = .1, col='black', fill='yellow')+ 
  theme(axis.text.x = element_blank(),axis.text.y = element_text(color = "black", size = 11,face = "bold"), 
        axis.title.y = element_blank())

