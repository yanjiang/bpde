{
#!/bin/bash
#copy this file (BPDE-dG_paired-end.sh) to the directory of paired-end *.fastq.gz files
#Keep file names in this format: basename_R1.fastq.gz & basename_R2.fastq.gz

mkdir fastqc_output

for i in $(find ./ -maxdepth 1 -type f -name "*.fastq.gz" | while read F; do basename -s .fastq.gz $F | rev | cut -c 4- | rev; done | sort | uniq)

    do 
		
		mkdir "$i"

		#trim common sequencing adapter contaminations
		bbduk.sh in1="$i"_R1.fastq.gz in2="$i"_R2.fastq.gz out1="$i"_output1_R1.fastq out2="$i"_output1_R2.fastq ref=adapters.fa 
		#filtering out reads containing AD1B sequence, corresponding to fully elongated DNA fragments (unspecific)
		bbduk.sh in1="$i"_output1_R1.fastq in2="$i"_output1_R2.fastq out1="$i"_output2_R1.fastq out2="$i"_output2_R2.fastq literal=GACTGGTTCCAATTGAAAGTGCTCTTCCGATCT hdist=2 
		#align to reference genome, in this case human hg38 using bwa, method = mem
		bwa mem -t 36 hg38.fa "$i"_output2_R1.fastq "$i"_output2_R2.fastq | samtools sort -@36 -o "$i"/"$i"_sorted.bam 
		#Deduplication: mark and remove PCR duplicates
		java -jar picard.jar MarkDuplicatesWithMateCigar I="$i"/"$i"_sorted.bam REMOVE_DUPLICATES=TRUE O="$i"/"$i"_Dedupedwithcigar.bam M="$i"_DedupwithCigar_metrics.txt 
		#output read1 only, due to the fact that the damage site is only on read 1 
		samtools view -F 4 -f 64 -@96 "$i"/"$i"_Dedupedwithcigar.bam -o "$i"/"$i"_R1_mapped_deduped.bam
		#transform the .bam file to .bed file
		bedtools bamtobed -i "$i"/"$i"_R1_mapped_deduped.bam > "$i"/"$i".bed 
		
		## (for plotting DNA logo) extracting position info for -10 to +10 and remove reads with start <0, otherwise next step will not proceed
		awk -v OFS='\t' '{if ($6=="+") {$3=$2+10;$2=$2-11} else {$2=$3-10;$3=$3+11}} {print}' "$i"/"$i".bed | awk -v OFS='\t' '$2>=0 {print}' > "$i"_21.bed 
		## get sequence of the defind 21nt based on reference genome
		bedtools getfasta -fi hg38.fa -bed "$i"_21.bed -bedOut -s | awk -v OFS='\t' '{print $1,$2,$3,$4,$5,$6,toupper($7)}' > "$i"/"$i"_21_seq.bed  
		## Calculating the PWM from -10 to +10 of the estimated damage site 
		awk '{print$7}' "$i"/"$i"_21_seq.bed | perl -ne 'chomp;@F=split(//);for$i(0..$#F){$k{$F[$i]}[$i]++}}{for $nt(sort keys(%k)){print"$nt\t";for$i(0..$#{$k{$nt}}){$g=$k{$nt}[$i]||0; printf"%.3f\t",$g/$.}print"\n";}' > "$i"/"$i"_21_PWM.txt  
		## remove intermediate files
		rm "$i"_output1_R1.fastq "$i"_output1_R2.fastq "$i"_output2_R1.fastq "$i"_output2_R2.fastq "$i"_21.bed 

		### (for tri-nucleotide damage-patterns) Print the three nucleotides context of the predicted site, damage at same site only count once
		awk '!a[$1$2$3$6]++' '{print substr($7,10,3)}' "$i"/"$i"_21_seq.bed | awk '{A[$1]++}END{for(i in A)print i,A[i]}' > "$i"_3nt.count
		awk -v OFS='\t' 'NR==FNR{a = a + $2;next} {c = ($2/a)*100;print $1,$2,c }' "$i"_3nt.count "$i"_3nt.count > "$i"/"$i"_3nt.txt
		rm "$i"_3nt.count


		#### (for further analysis in bins) extract the predicted damage position (-1) information and remove reads start <0
		awk -v OFS='\t' '{if ($6=="+") {$3=$2;$2=$2-1} else {$2=$3;$3=$3+1}} {print}' "$i"/"$i".bed | awk -v OFS='\t' '$2>=0 {print}' > "$i"_1.bed 
		#### get sequence of the defind 1nt based on reference genome
		bedtools getfasta -fi hg38.fa -bed "$i"_1.bed -bedOut -s | awk -v OFS='\t' '{print $1,$2,$3,$4,$5,$6,toupper($7)}' > "$i"_1_seq.bed 
		#### filter the file to leave only G at the predicted position
		awk -v OFS='\t' '{if ($7=="G") print}' "$i"_1_seq.bed | sort -k1,1 -k2,2n > "$i"/"$i"_1G.bed
		#### calculate the density/coverage of the predicted sites in defined bins
		bedtools coverage -a 100kb_BlacklistRemoved.bed -b "$i"/"$i"_1G.bed > "$i"/"$i"_1G.coverage
		#### Ouput the density data in bedgragph format 
		awk -v OFS='\t' '{print $1,$2,$3,$4}' "$i"/"$i"_1G.coverage > "$i"/"$i"_1G.density
		#### remove intermediate files
		rm "$i"_1.bed "$i"_1_seq.bed


	done;
}
